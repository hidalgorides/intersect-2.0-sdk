<?php

namespace Intersect\SDK\Medium;

use Intersect\SDK\AccessTokenDetails;
use Intersect\SDK\Medium\Entity\Contributor;
use Intersect\SDK\Medium\Entity\Post;
use Intersect\SDK\Medium\Entity\Publication;
use Intersect\SDK\Medium\Entity\UserDetails;

class Medium {

    /** @var MediumHttpClient */
    private $httpClient;

    public function __construct($options)
    {
        $this->httpClient = new MediumHttpClient($options);
    }

    public function setAccessToken($accessToken)
    {
        $this->httpClient->setAccessToken($accessToken);
    }

    /**
     * @param $code
     * @return AccessTokenDetails
     * @throws \Intersect\SDK\HttpClientException
     */
    public function authenticate($code)
    {
        $responseObject = $this->httpClient->getAccessTokenDetails($code);

        $accessTokenDetails = new AccessTokenDetails();

        $accessTokenDetails->setAccessToken($responseObject->access_token);
        $accessTokenDetails->setType($responseObject->token_type);
        $accessTokenDetails->setRefreshToken($responseObject->refresh_token);
        $accessTokenDetails->setExpires($responseObject->expires_at);

        return $accessTokenDetails;
    }

    /**
     * @param $refreshToken
     * @return mixed
     * @throws \Intersect\SDK\HttpClientException
     */
    public function refreshAccessToken($refreshToken)
    {
        $responseObject = $this->httpClient->refreshAccessToken($refreshToken);
        return $responseObject->access_token;
    }

    /**
     * @param null $state
     * @return string
     */
    public function getAuthorizationnUrl($state = null)
    {
        $state = (is_null($state) ? time() : $state);
        return $this->httpClient->generateAuthorizationUrl($state);
    }

    /**
     * @return UserDetails
     * @throws \Intersect\SDK\HttpClientException
     */
    public function getUserDetails()
    {
        $responseObject = $this->httpClient->getUserDetails();
        $responseData = $responseObject->data;

        $userDetails = new UserDetails();

        $userDetails->setId($responseData->id);
        $userDetails->setUsername($responseData->username);
        $userDetails->setName($responseData->name);
        $userDetails->setUrl($responseData->url);
        $userDetails->setImageUrl($responseData->imageUrl);

        return $userDetails;
    }

    /**
     * @param $userId
     * @return Publication[]
     * @throws \Intersect\SDK\HttpClientException
     */
    public function getPublications($userId)
    {
        $responseObject = $this->httpClient->getPublications($userId);
        $responseData = $responseObject->data;

        $publications = [];
        foreach ($responseData as $data)
        {
            $publication = new Publication();
            $publication->setId($data->id);
            $publication->setName($data->name);
            $publication->setDescription($data->description);
            $publication->setUrl($data->url);
            $publication->setImageUrl($data->imageUrl);

            $publications[] = $publication;
        }
        return $publications;
    }

    /**
     * @param $publicationId
     * @return Contributor[]
     * @throws \Intersect\SDK\HttpClientException
     */
    public function getContributors($publicationId)
    {
        $responseObject = $this->httpClient->getContributors($publicationId);
        $responseData = $responseObject->data;

        $contributors = [];
        foreach ($responseData as $data)
        {
            $contributor = new Contributor();
            $contributor->setPublicationId($data->publicationId);
            $contributor->setUserId($data->userId);
            $contributor->setRole($data->role);

            $contributors[] = $contributor;
        }
        return $contributors;
    }

    /**
     * @param Post $post
     * @param $userId
     * @return Post
     * @throws \Intersect\SDK\HttpClientException
     */
    public function createPost(Post $post, $userId)
    {
        $postData = [
            'title' => $post->getTitle(),
            'contentFormat' => $post->getContentFormat(),
            'content' => $post->getContent(),
            'canonicalUrl' => $post->getCanonicalUrl(),
            'tags' => $post->getTags(),
            'publishStatus' => $post->getPublishStatus(),
            'license' => $post->getLicense(),
            'notifyFollowers' => $post->getNotifyFollowers()
        ];

        $responseObject = $this->httpClient->createPost($userId, $postData);
        $responseData = $responseObject->data;

        return $this->buildPostFromResponseData($responseData);
    }

    /**
     * @param Post $post
     * @param $publicationId
     * @return Post
     * @throws \Intersect\SDK\HttpClientException
     */
    public function createPostForPublication(Post $post, $publicationId)
    {
        $postData = [
            'title' => $post->getTitle(),
            'contentFormat' => $post->getContentFormat(),
            'content' => $post->getContent(),
            'canonicalUrl' => $post->getCanonicalUrl(),
            'tags' => $post->getTags(),
            'publishStatus' => $post->getPublishStatus(),
            'license' => $post->getLicense(),
            'notifyFollowers' => $post->getNotifyFollowers()
        ];

        $responseObject = $this->httpClient->createPostForPublication($publicationId, $postData);
        $responseData = $responseObject->data;

        return $this->buildPostFromResponseData($responseData);
    }

    /**
     * @param $responseData
     * @return Post
     */
    private function buildPostFromResponseData($responseData)
    {
        $post = new Post();
        $post->setId($responseData->id);
        $post->setTitle($responseData->title);
        $post->setAuthorId($responseData->authorId);
        $post->setUrl($responseData->url);
        $post->setCanonicalUrl($responseData->canonicalUrl);
        $post->setPublishStatus($responseData->publishStatus);

        if (property_exists($responseData, 'publishedAt'))
        {
        $post->setPublishedAt($responseData->publishedAt);
        }

        $post->setLicense($responseData->license);
        $post->setLicenseUrl($responseData->licenseUrl);

        $tags = $responseData->tags;
        if (!is_null($tags))
        {
            $post->setTags($tags);
        }

        return $post;
    }

}