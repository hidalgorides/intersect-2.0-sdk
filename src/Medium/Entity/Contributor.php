<?php

namespace Intersect\SDK\Medium\Entity;

class Contributor {

    private $publicationId;
    private $userId;
    private $role;

    public function getPublicationId()
    {
        return $this->publicationId;
    }

    public function setPublicationId($publicationId)
    {
        $this->publicationId = $publicationId;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;
    }

}