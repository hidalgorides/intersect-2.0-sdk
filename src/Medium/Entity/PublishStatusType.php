<?php

namespace Intersect\SDK\Medium\Entity;

class PublishStatusType {
    const PUBLIC = 'public';
    const DRAFT = 'draft';
    const UNLISTED = 'unlisted';
}