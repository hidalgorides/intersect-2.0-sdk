# Medium SDK

__Namespace:__ 
Intersect\SDK\Medium

The Medium SDK is used for integrating your site with the Medium API for retrieving user details, publishing posts, retrieving publication lists, and retrieving contributors. 

## Prerequisites

Prior to using this SDK, you will need to do either of the following steps
- Create an integration token (some features not available when only using an integration token)
- Setup a developer application

## Create a new Medium instance

You have two options for creating your _Medium_ instance depending on how you chose to setup your Medium account, developer application vs integration token

### Option #1: Integration Token (easiest)
```php
<?php
$integrationToken = 'INSERT_INTEGRATION_TOKEN_VALUE';

$medium = new \Intersect\SDK\Medium\Medium($integrationToken);
``` 

### Option #2: Developer Application
```php
<?php

$medium = new \Intersect\SDK\Medium\Medium([
'client-id' => 'INSERT_CLIENT_ID',
'client-secret' => 'INSERT_CLIENT_SECRET',
'redirect-uri' => 'INSERT_REDIRECT_URI'
]);
```

## Obtaining an access token
__Note: only needed when using a developer application__

Generate the authorization URL
```php
<?php

$state = 'INSERT_STATE';
echo $medium->getAuthorizationnUrl($state);
```

Once you allow access, you will be redirected back to the redirect URI you set in your Medium instance configuration (redirect-uri).

After being redirected, you will have access to the _state_ and _code_ query parameters in the URL. You can use the _state_ value to confirm the request is coming from the state you initial sent with the authorization request. You will need to retrieve the _code_ value from the URL in order to retrieve your new access token details.

Example URL: https://example.com/?state=your-state&code=593fh60hf

Once you have verified the state, if needed, you can proceed to use the _code_ value to obtain your new access token details

```php
<?php

$code = $_GET['code'];

$accessTokenDetails = $medium->authenticate($code);

var_dump($accessTokenDetails);
``` 

You can now use the access token details to to retrieve the actual access token needed to perform further operations. You may store these details however you like to use them for repeated calls, but use your discretion when storing any data.

```php
<?php

$medium->setAccessToken($accessTokenDetails->getAccessToken());
```

## Operations

### Getting user details
```php
<?php

$userDetails = $medium->getUserDetails();

var_dump($userDetails);
```

### Creating new post
```php
<?php

$post = new \Intersect\SDK\Medium\Entity\Post();
$post->setTitle('First Post');
$post->setContent('This is my first post');
$post->setContentFormat('html'); // 'html' or 'markdown' allowed

$userId = $userDetails->getId();

$createdPost = $medium->createPost($post, $userId);

echo $createdPost->getUrl();

var_dump($createdPost);
```