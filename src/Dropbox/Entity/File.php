<?php

namespace Intersect\SDK\Dropbox\Entity;

class File {

    private $hash;
    private $id;
    private $modifiedDateClient;
    private $modifiedDateServer;
    private $name;
    private $pathLower;
    private $pathDisplay;
    private $revision;
    private $size;

    public function getHash()
    {
        return $this->hash;
    }

    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getModifiedDateClient()
    {
        return $this->modifiedDateClient;
    }

    public function setModifiedDateClient($modifiedDateClient)
    {
        $this->modifiedDateClient = $modifiedDateClient;
    }

    public function getModifiedDateServer()
    {
        return $this->modifiedDateServer;
    }

    public function setModifiedDateServer($modifiedDateServer)
    {
        $this->modifiedDateServer = $modifiedDateServer;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getPathLower()
    {
        return $this->pathLower;
    }

    public function setPathLower($pathLower)
    {
        $this->pathLower = $pathLower;
    }

    public function getPathDisplay()
    {
        return $this->pathDisplay;
    }

    public function setPathDisplay($pathDisplay)
    {
        $this->pathDisplay = $pathDisplay;
    }

    public function getRevision()
    {
        return $this->revision;
    }

    public function setRevision($revision)
    {
        $this->revision = $revision;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size)
    {
        $this->size = $size;
    }

}