<?php

namespace Intersect\SDK\Dropbox\Entity;

class Directory {

    private $id;
    private $name;
    private $pathLower;
    private $pathDisplay;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getPathLower()
    {
        return $this->pathLower;
    }

    public function setPathLower($pathLower)
    {
        $this->pathLower = $pathLower;
    }

    public function getPathDisplay()
    {
        return $this->pathDisplay;
    }

    public function setPathDisplay($pathDisplay)
    {
        $this->pathDisplay = $pathDisplay;
    }

}