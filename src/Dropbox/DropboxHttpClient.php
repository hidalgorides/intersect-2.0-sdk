<?php

namespace Intersect\SDK\Dropbox;

use Intersect\SDK\HttpClient;
use Intersect\SDK\HttpRequestData;

class DropboxHttpClient extends HttpClient {

    private static $AUTHORIZE_URL_ENDPOINT = 'https://www.dropbox.com/oauth2/authorize';
    private static $API_BASE_ENDPOINT = 'https://api.dropboxapi.com';
    private static $CONTENT_BASE_ENDPOINT = 'https://content.dropboxapi.com';

    /**
     * @param $state
     * @return string
     */
    public function generateAuthorizationUrl($state)
    {
        $queryParameters = [
            'client_id' => $this->getClientId(),
            'scope' => implode(',', $this->getScopes()),
            'state' => $state,
            'response_type' => 'code',
            'redirect_uri' => $this->getRedirectUri()
        ];

        return self::$AUTHORIZE_URL_ENDPOINT . '?' . http_build_query($queryParameters);
    }

    public function getAccessTokenDetails($code) 
    {
        $requestData = new HttpRequestData();

        $requestData->setFormData([
            'code' => $code,
            'client_id' => $this->getClientId(),
            'client_secret' => $this->getClientSecret(),
            'grant_type' => 'authorization_code',
            'redirect_uri' => $this->getRedirectUri(),
        ]);

        $response = $this->request('POST', self::$API_BASE_ENDPOINT . '/oauth2/token', $requestData);

        return json_decode((string) $response->getBody());
    }

    public function refreshAccessToken($refreshToken) 
    {
        throw new \Exception('Method not supported');
    }

    public function createDirectory($path)
    {
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->getAccessToken()
        ];

        $payload = [
            'path' => $path,
            'autorename' => true
        ];

        $requestData = new HttpRequestData();
        $requestData->setHeaders($headers);
        $requestData->setJsonData($payload);

        $response = $this->request('POST', self::$API_BASE_ENDPOINT . '/2/files/create_folder_v2', $requestData);

        return json_decode((string) $response->getBody());
    }

    public function listDirectoryContents($path)
    {
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->getAccessToken()
        ];

        $payload = [
            'path' => $path,
        ];

        $requestData = new HttpRequestData();
        $requestData->setHeaders($headers);
        $requestData->setJsonData($payload);

        $response = $this->request('POST', self::$API_BASE_ENDPOINT . '/2/files/list_folder', $requestData);

        return json_decode((string) $response->getBody());
    }

    public function deleteEntity($entityPath)
    {
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->getAccessToken()
        ];

        $payload = [
            'path' => $entityPath
        ];

        $requestData = new HttpRequestData();
        $requestData->setHeaders($headers);
        $requestData->setJsonData($payload);

        $response = $this->request('POST', self::$API_BASE_ENDPOINT . '/2/files/delete_v2', $requestData);

        return json_decode((string) $response->getBody());
    }

    public function uploadFile($filePath, $uploadPath)
    {
        $uploadArguments = [
            'path' => $uploadPath,
            'mode' => 'overwrite'
        ];

        $headers = [
            'Content-Type' => 'application/octet-stream',
            'Authorization' => 'Bearer ' . $this->getAccessToken(),
            'Dropbox-API-Arg' => json_encode($uploadArguments)
        ];

        $payload = [
            'name' => $name,
            'contents' => $filePath
        ];

        $requestData = new HttpRequestData();
        $requestData->setHeaders($headers);
        $requestData->setBody(fopen($filePath, 'r'));

        $response = $this->request('POST', self::$CONTENT_BASE_ENDPOINT . '/2/files/upload', $requestData);

        return json_decode((string) $response->getBody());
    }

}