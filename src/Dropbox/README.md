# Dropbox SDK

__Namespace:__ 
Intersect\SDK\Dropbox

The Dropbox SDK is used for integrating your site with the Dropbox API for retrieving directory contents, creating directories, deleting files, deleting directories, and uploading new files.

## Prerequisites

Prior to using this SDK, you will need to do either of the following steps
- Create an integration token (some features not available when only using an integration token)
- Setup a developer application

## Create a new Dropbox instance

```php
<?php

$dropbox = new \Intersect\SDK\Dropbox\Dropbox([
'client-id' => 'INSERT_CLIENT_ID',
'client-secret' => 'INSERT_CLIENT_SECRET',
'redirect-uri' => 'INSERT_REDIRECT_URI'
]);
```

## Obtaining an access token

Generate the authorization URL
```php
<?php

$state = 'INSERT_STATE';
echo $dropbox->getAuthorizationnUrl($state);
```

Once you allow access, you will be redirected back to the redirect URI you set in your Dropbox instance configuration (redirect-uri).

After being redirected, you will have access to the _state_ and _code_ query parameters in the URL. You can use the _state_ value to confirm the request is coming from the state you initial sent with the authorization request. You will need to retrieve the _code_ value from the URL in order to retrieve your new access token details.

Example URL: https://example.com/?state=your-state&code=593fh60hf

Once you have verified the state, if needed, you can proceed to use the _code_ value to obtain your new access token details

```php
<?php

$code = $_GET['code'];

$accessTokenDetails = $dropbox->authenticate($code);

var_dump($accessTokenDetails);
``` 

You can now use the access token details to to retrieve the actual access token needed to perform further operations. You may store these details however you like to use them for repeated calls, but use your discretion when storing any data.

```php
<?php

$dropbox->setAccessToken($accessTokenDetails->getAccessToken());
```

## Operations

### Creating a directory
```php
<?php

$createdDirectory = $dropbox->createDirectory($rootPath, $directoryName)

var_dump($createdDirectory);
```