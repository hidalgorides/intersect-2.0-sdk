<?php

namespace Intersect\SDK\Dropbox;

use Intersect\SDK\AccessTokenDetails;
use Intersect\SDK\Dropbox\DropboxHttpClient;
use Intersect\SDK\Dropbox\Entity\Directory;
use Intersect\SDK\Dropbox\Entity\File;

class Dropbox {

    /** @var DropboxHttpClient */
    private $httpClient;

    public function __construct($options)
    {
        $this->httpClient = new DropboxHttpClient($options);
    }

    public function setAccessToken($accessToken)
    {
        $this->httpClient->setAccessToken($accessToken);
    }

    /**
     * @param null $state
     * @return string
     */
    public function getAuthorizationnUrl($state = null)
    {
        $state = (is_null($state) ? time() : $state);
        return $this->httpClient->generateAuthorizationUrl($state);
    }

     /**
     * @param $code
     * @return AccessTokenDetails
     * @throws \Intersect\SDK\HttpClientException
     */
    public function authenticate($code)
    {
        $responseObject = $this->httpClient->getAccessTokenDetails($code);

        $accessTokenDetails = new AccessTokenDetails();

        $accessTokenDetails->setAccessToken($responseObject->access_token);
        $accessTokenDetails->setType($responseObject->token_type);

        return $accessTokenDetails;
    }

    /**
     * @param $rootPath
     * @param $directoryName
     * @return Directory
     */
    public function createDirectory($rootPath, $directoryName)
    {
        $path = rtrim($rootPath, '/') . '/' . $directoryName;

        $responseObject = $this->httpClient->createDirectory($path);
        $responseData = $responseObject->metadata;

        $createdDirectory = new Directory();
        $createdDirectory->setId($responseData->id);
        $createdDirectory->setName($responseData->name);
        $createdDirectory->setPathLower($responseData->path_lower);
        $createdDirectory->setPathDisplay($responseData->path_display);

        return $createdDirectory;
    }

    public function deleteFile($filePath)
    {
        $responseObject = $this->httpClient->deleteEntity($filePath);

        return (!is_null($responseObject));
    }

    public function deleteDirectory($directoryPath)
    {
        $responseObject = $this->httpClient->deleteEntity($directoryPath);

        return (!is_null($responseObject));
    }

    /**
     * @param $filePath
     * @param $uploadDirectory
     * @throws Exception
     * @return File
     */
    public function uploadFile($filePath, $uploadDirectory, $fileNameOverride = null)
    {
        $fileName = $fileNameOverride;

        if (is_null($fileName))
        {
            $fileName = end(explode('/', $filePath));
        }

        $uploadPath = rtrim($uploadDirectory, '/') . '/' . $fileName;
        
        $responseObject = $this->httpClient->uploadFile($filePath, $uploadPath);

        if (is_null($responseObject))
        {
            throw new \Exception('There was a problem uploading your file to Dropbox');
        }

        $uploadedFile = new File();
        $uploadedFile->setId($responseObject->id);
        $uploadedFile->setHash($responseObject->content_hash);
        $uploadedFile->setName($responseObject->name);
        $uploadedFile->setPathDisplay($responseObject->path_display);
        $uploadedFile->setPathLower($responseObject->path_lower);
        $uploadedFile->setModifiedDateClient($responseObject->client_modified);
        $uploadedFile->setModifiedDateServer($responseObject->server_modified);
        $uploadedFile->setRevision($responseObject->rev);
        $uploadedFile->setSize($responseObject->size);

        return $uploadedFile;
    }

    /**
     * @param $path
     * @param $recursive
     * @param $depth
     * @return File[]
     */
    public function getDirectoryContents($path, $recursive = false, $depth = null)
    {
        $responseObject = $this->httpClient->listDirectoryContents($path);
        
        $entries = $responseObject->entries;

        $allFiles = [];

        foreach ($entries as $entry)
        {
            if ($entry->{'.tag'} == 'file')
            {
                $file = new File();
                $file->setId($entry->id);
                $file->setHash($entry->content_hash);
                $file->setName($entry->name);
                $file->setPathDisplay($entry->path_display);
                $file->setPathLower($entry->path_lower);
                $file->setModifiedDateClient($entry->client_modified);
                $file->setModifiedDateServer($entry->server_modified);
                $file->setRevision($entry->rev);
                $file->setSize($entry->size);

                $allFiles[] = $file;
            }
        }

        return $allFiles;
    }

}