<?php

namespace Intersect\SDK;

interface OAuthClient {

    public function generateAuthorizationUrl($state);

    public function getAccessTokenDetails($code);

    public function refreshAccessToken($refreshToken);

    public function setAccessToken($accessToken);

}