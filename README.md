# Intersect SDK
**Intersect SDK** contains SDKs for integrating with various platforms

## Changelog
See `CHANGELOG.md` for all current and released features/changes

## Source
https://bitbucket.org/hidalgorides/intersect-2.0-sdk

## Installation via Composer
Include the following code snippet inside your project `composer.json` file (update if necessary)
```
"repositories": [
  {
    "type": "vcs",
    "url": "https://bitbucket.org/hidalgorides/intersect-2.0-sdk"
  }
],
"require" : {
  "intersect/sdk" : "^1.0.0"
}
```